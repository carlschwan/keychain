// SPDX-FileCopyrightText: 2024 Carl Schwan <carl@carlschwan.eu>
// SPDX-License-Identifier: LGPL-2.0-or-later

#pragma once

#include "core/Database.h"
#include <QObject>

class DatabaseEditorBackend : public QObject
{
    Q_OBJECT

    Q_PROPERTY(Database *database READ database WRITE setDatabase NOTIFY databaseChanged)

public:
    explicit DatabaseEditorBackend(QObject *parent = nullptr);

    Database *database() const;
    void setDatabase(Database *database);

Q_SIGNALS:
    void databaseChanged();

private:
    Database *m_database = nullptr;
};