/*
 *  Copyright (C) 2011 Felix Geyer <debfx@fobos.de>
 *  Copyright (C) 2017 KeePassXC Team <team@keepassxc.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 or (at your option)
 *  version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "core/Database.h"
#include "keys/drivers/YubiKey.h"
#include <QPointer>
#include <QScopedPointer>
#include <QTimer>
#include <QUrl>
#include <QtQml/qqmlregistration.h>

class CompositeKey;
class QFile;

class DatabaseOpenHelper : public QObject
{
    Q_OBJECT
    QML_ELEMENT

    /// This property holds the path to the database.
    Q_PROPERTY(QUrl databaseUrl READ databaseUrl WRITE setDatabaseUrl NOTIFY databaseUrlChanged)

    /// This property holds the path to the keyFile.
    Q_PROPERTY(QUrl keyFileUrl READ keyFileUrl WRITE setKeyFileUrl NOTIFY keyFileUrlChanged)

    /// This property holds the password for the database.
    Q_PROPERTY(QString password READ password WRITE setPassword NOTIFY passwordChanged)

    /// This property holds the index of the yubikey
    Q_PROPERTY(YubiKeySlot yubikeySlot READ yubikeySlot WRITE setYubikeySlot NOTIFY yubikeySlotChanged RESET resetYubikeySlot)

    /// This property holds the password for the database.
    Q_PROPERTY(bool userInteractionLock READ userInteractionLock WRITE setUserInteractionLock NOTIFY userInteractionLockChanged)
public:
    enum MessageType {
        Information = 0,
        Positive,
        Warning,
        Error,
    };
    Q_ENUM(MessageType);

    explicit DatabaseOpenHelper(QObject *parent = nullptr);
    ~DatabaseOpenHelper() override;

    [[nodiscard]] QUrl databaseUrl() const;
    void setDatabaseUrl(const QUrl &databaseUrl);

    [[nodiscard]] QUrl keyFileUrl() const;
    void setKeyFileUrl(const QUrl &keyFileUrl);

    [[nodiscard]] QString password() const;
    void setPassword(const QString &password);

    [[nodiscard]] bool userInteractionLock() const;
    void setUserInteractionLock(bool state);

    YubiKeySlot yubikeySlot() const;
    void setYubikeySlot(YubiKeySlot yubiKeySlot);
    void resetYubikeySlot();

    Database *database() const;
    bool unlockingDatabase() const;

    // Quick Unlock helper functions
    [[nodiscard]] bool canPerformQuickUnlock() const;
    [[nodiscard]] bool isOnQuickUnlockScreen() const;
    void toggleQuickUnlockScreen();
    void triggerQuickUnlock();
    void resetQuickUnlock();

public Q_SLOTS:
    void cancelDatabaseUnlockSlot();
    void saveQuickUnlockCredentialSlot(bool blockQuickUnlock, const QSharedPointer<CompositeKey> &databaseKey);
    void openDatabase();
    void retryUnlockWithEmptyPassword();

Q_SIGNALS:
    void finished(Database *database);
    void unlockFailedNoPassword();
    void hideMessage();
    void showMessage(const QString &message, MessageType type);
    void databaseUrlChanged();
    void keyFileUrlChanged();
    void passwordChanged();
    void hasMinorVersionMismatch(bool blockQuickUnlock, const QSharedPointer<CompositeKey> &databaseKey);
    void userInteractionLockChanged();
    void yubikeySlotChanged();

protected:
    bool event(QEvent *event) override;
    QSharedPointer<CompositeKey> buildDatabaseKey();

    std::unique_ptr<Database> m_db;
    QUrl m_datbaseUrl;
    bool m_retryUnlockWithEmptyPassword = false;

private:
    bool m_pollingHardwareKey = false;
    bool m_manualHardwareKeyRefresh = false;
    bool m_blockQuickUnlock = false;
    bool m_unlockingDatabase = false;
    QTimer m_hideTimer;
    QTimer m_hideNoHardwareKeysFoundTimer;
    QUrl m_databaseUrl;
    QUrl m_keyFileUrl;
    QString m_password;
    std::optional<YubiKeySlot> m_yubikeySlot;

    Q_DISABLE_COPY(DatabaseOpenHelper)
};
