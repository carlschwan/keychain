// SPDX-FileCopyrightText: 2011 Felix Geyer <debfx@fobos.de>
// SPDX-FileCopyrightText: 2022 KeePassXC Team <team@keepassxc.org>
// SPDX-FileCopyrightText: 2024 Carl Schwan <carl@carlschwan.eu>
// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only

#include "groupeditorbackend.h"

GroupEditorBackend::GroupEditorBackend(QObject *parent)
    : QObject(parent)
{
}

void GroupEditorBackend::loadGroup(Group *group, Group *parentGroup)
{
    Q_ASSERT(group || parentGroup);

    m_group = group;
    m_parentGroup = parentGroup;

    if (!m_group) {
        m_group = new Group();
        m_group->setUuid(QUuid::createUuid());
        m_newGroup = true;
    } else {
        m_newGroup = false;
    }
    m_temporaryGroup.reset(m_group->clone(Entry::CloneNoFlags, Group::CloneNoFlags));
    connect(m_temporaryGroup->customData(), &CustomData::modified, this, [this]() {
        setModified(true);
    });

    setModified(false);

    Q_EMIT newGroupChanged();
    Q_EMIT groupChanged();
}

Group *GroupEditorBackend::group() const
{
    return m_temporaryGroup.get();
}

bool GroupEditorBackend::isNewGroup() const
{
    return m_newGroup;
}

bool GroupEditorBackend::isModified() const
{
    return m_modified;
}

void GroupEditorBackend::setModified(bool modified)
{
    if (modified == m_modified) {
        return;
    }
    m_modified = modified;
    Q_EMIT modifiedChanged();
}

void GroupEditorBackend::cancel()
{
    if (m_parentGroup) {
        delete m_group;
    }
    clear();
    Q_EMIT editFinished(false);
}

void GroupEditorBackend::save()
{
    apply();
    clear();
    Q_EMIT editFinished(true);
}

void GroupEditorBackend::apply()
{
    m_group->copyDataFrom(m_temporaryGroup.get());

    if (m_parentGroup) {
        m_group->setParent(m_parentGroup);
        m_group->database()->save();
    }

    setModified(false);
}

void GroupEditorBackend::clear()
{
    m_group = nullptr;
    m_db = nullptr;
    m_temporaryGroup.reset(nullptr);
}
