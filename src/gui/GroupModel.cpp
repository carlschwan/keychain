// SPDX-FileCopyrightText: 2010 Felix Geyer <debfx@fobos.de>
// SPDX-FileCopyrightText: 2024 Carl Schwan <carl@carlschwan.eu>
// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only

#include "GroupModel.h"

#include <QFont>
#include <QMimeData>

#include "core/Database.h"
#include "core/Group.h"
#include "core/Metadata.h"
#include "core/Tools.h"
#include "keeshare/KeeShare.h"

using namespace Qt::StringLiterals;

GroupModel::GroupModel(QObject *parent)
    : QAbstractItemModel(parent)
{
}

Database *GroupModel::database() const
{
    return m_db;
}

void GroupModel::setDatabase(Database *newDb)
{
    if (m_db == newDb) {
        return;
    }

    if (m_db) {
        disconnect(m_db, &Database::groupDataChanged, this, nullptr);
    }

    beginResetModel();

    m_db = newDb;
    if (m_db) {
        connect(m_db, &Database::groupDataChanged, this, &GroupModel::groupDataChanged);
        connect(m_db, &Database::groupAboutToAdd, this, &GroupModel::groupAboutToAdd);
        connect(m_db, &Database::groupAdded, this, &GroupModel::groupAdded);
        connect(m_db, &Database::groupAboutToRemove, this, &GroupModel::groupAboutToRemove);
        connect(m_db, &Database::groupRemoved, this, &GroupModel::groupRemoved);
        connect(m_db, &Database::groupAboutToMove, this, &GroupModel::groupAboutToMove);
        connect(m_db, &Database::groupMoved, this, &GroupModel::groupMoved);
    }

    endResetModel();

    Q_EMIT databaseChanged();
}

int GroupModel::rowCount(const QModelIndex &parent) const
{
    if (!m_db) {
        return 0;
    }
    if (!parent.isValid()) {
        // we have exactly 1 root item
        return 1;
    } else {
        const Group *group = groupFromIndex(parent);
        return group->children().size();
    }
}

int GroupModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    if (!m_db) {
        return 0;
    }

    return 1;
}

QModelIndex GroupModel::index(int row, int column, const QModelIndex &parent) const
{
    if (!hasIndex(row, column, parent)) {
        return {};
    }

    Group *group;

    if (!parent.isValid()) {
        group = m_db->rootGroup();
    } else {
        group = groupFromIndex(parent)->children().at(row);
    }

    return createIndex(row, column, group);
}

QModelIndex GroupModel::parent(const QModelIndex &index) const
{
    if (!index.isValid()) {
        return {};
    }

    return parent(groupFromIndex(index));
}

QModelIndex GroupModel::parent(Group *group) const
{
    Group *parentGroup = group->parentGroup();

    if (!parentGroup) {
        // index is already the root group
        return {};
    } else {
        const Group *grandParentGroup = parentGroup->parentGroup();
        if (!grandParentGroup) {
            // parent is the root group
            return createIndex(0, 0, parentGroup);
        } else {
            return createIndex(grandParentGroup->children().indexOf(parentGroup), 0, parentGroup);
        }
    }
}

QHash<int, QByteArray> GroupModel::roleNames() const
{
    return {
        {GroupNameRole, "groupName"},
        {IsExpiredRole, "isExpired"},
        {IconNameRole, "iconName"},
        {GroupRole, "group"},
    };
}

QVariant GroupModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return {};
    }

    Group *group = groupFromIndex(index);

    switch (role) {
    case Qt::DisplayRole:
    case GroupNameRole: {
        QString nameTemplate = u"%1"_s;
        nameTemplate = KeeShare::indicatorSuffix(group, nameTemplate);
        return nameTemplate.arg(group->name());
    }
    case IconNameRole:
        if (group->iconUuid().isNull()) {
            switch (group->iconNumber()) {
                // match https://github.com/keepassxreboot/keepassxc/tree/develop/share/icons/database
            case 0:
                return u"lock-symbolic"_s;
            case 1:
                return u"preferences-system-network-sharing"_s;
            case 2:
                return u"data-warning"_s;
            case 3:
                return u"preferences-system-network-server"_s;
            case 4:
                return u"edit-paste-symbolic"_s;
            case 5:
                return u"folder-language-symbolic"_s;
            case 6:
                return u"configure-symbolic"_s;
            case 7:
                return u"story-editor-symbolic"_s;
            case 8:
                return u"cloud-upload-symbolic"_s;
            case 9:
                return u"identity-symbolic"_s;
            case 10:
                return u"view-pim-contacts-symbolic"_s;
            case 11:
                return u"camera-video-symbolic"_s;
            case 12:
                return u"folder-cloud-symbolic"_s;
            case 13:
                return u"unlock-symbolic"_s;
            case 14:
                return u"battery-050-charging-symbolic"_s;
            case 15:
                return u"bookmarks-symbolic"_s;
            case 16:
                return u"scanner-symbolic"_s;
            case 17:
                return u"media-optical-audio-symbolic"_s;
            case 18:
                return u"folder-mail-symbolic"_s;
            case 19:
                return u"folder-mail-symbolic"_s;
            case 20:
                return u"configure-symbolic"_s;
            case 43:
                return u"user-trash-full-symbolic"_s;
            case 49:
                return u"folder-symbolic"_s;
            default:
                qWarning() << "TODO complete list of icons";
            }
        } else if (group->database()) {
            return u"folder-symbolic"_s;
        }
        return u"folder-symbolic"_s;
    case GroupRole:
        return QVariant::fromValue(group);
    case Qt::FontRole: {
        QFont font;
        if (group->isExpired()) {
            font.setStrikeOut(true);
        }
        return font;
    }
    case IsExpiredRole:
        return group->isExpired();
    case Qt::ToolTipRole: {
        QString tooltip;
        if (!group->parentGroup()) {
            // only show a tooltip for the root group
            tooltip = m_db->filePath();
        }
        return tooltip;
    }
    default:
        return {};
    }
}

QModelIndex GroupModel::index(Group *group) const
{
    int row;

    if (!group->parentGroup()) {
        row = 0;
    } else {
        row = group->parentGroup()->children().indexOf(group);
    }

    return createIndex(row, 0, group);
}

Group *GroupModel::groupFromIndex(const QModelIndex &index) const
{
    Q_ASSERT(index.internalPointer());

    return static_cast<Group *>(index.internalPointer());
}

Qt::DropActions GroupModel::supportedDropActions() const
{
    return Qt::MoveAction | Qt::CopyAction;
}

Qt::ItemFlags GroupModel::flags(const QModelIndex &modelIndex) const
{
    if (!modelIndex.isValid()) {
        return Qt::NoItemFlags;
    } else if (modelIndex == index(0, 0)) {
        return QAbstractItemModel::flags(modelIndex) | Qt::ItemIsDropEnabled;
    } else {
        return QAbstractItemModel::flags(modelIndex) | Qt::ItemIsDropEnabled | Qt::ItemIsDragEnabled;
    }
}

bool GroupModel::dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent)
{
    Q_UNUSED(column);

    if (action == Qt::IgnoreAction) {
        return true;
    }

    if (!data || (action != Qt::MoveAction && action != Qt::CopyAction) || !parent.isValid()) {
        return false;
    }

    // check if the format is supported
    QStringList types = mimeTypes();
    Q_ASSERT(types.size() == 2);
    bool isGroup = data->hasFormat(types.at(0));
    bool isEntry = data->hasFormat(types.at(1));
    if (!isGroup && !isEntry) {
        return false;
    }

    if (row > rowCount(parent)) {
        row = rowCount(parent);
    }

    // decode and insert
    QByteArray encoded = data->data(isGroup ? types.at(0) : types.at(1));
    QDataStream stream(&encoded, QIODevice::ReadOnly);

    Group *parentGroup = groupFromIndex(parent);

    if (isGroup) {
        QUuid dbUuid;
        QUuid groupUuid;
        stream >> dbUuid >> groupUuid;

        Database *db = Database::databaseByUuid(dbUuid);
        if (!db) {
            return false;
        }

        Group *dragGroup = db->rootGroup()->findGroupByUuid(groupUuid);
        if (!dragGroup || !db->rootGroup()->findGroupByUuid(dragGroup->uuid()) || dragGroup == db->rootGroup()) {
            return false;
        }

        if (dragGroup == parentGroup || dragGroup->findGroupByUuid(parentGroup->uuid())) {
            return false;
        }

        if (parentGroup == dragGroup->parent() && row > parentGroup->children().indexOf(dragGroup)) {
            row--;
        }

        Database *sourceDb = dragGroup->database();
        Database *targetDb = parentGroup->database();

        Group *group = dragGroup;

        if (sourceDb != targetDb) {
            QSet<QUuid> customIcons = group->customIconsRecursive();
            targetDb->metadata()->copyCustomIcons(customIcons, sourceDb->metadata());

            // Always clone the group across db's to reset UUIDs
            group = dragGroup->clone(Entry::CloneDefault | Entry::CloneIncludeHistory);
            if (action == Qt::MoveAction) {
                // Remove the original group from the sourceDb
                delete dragGroup;
            }
        } else if (action == Qt::CopyAction) {
            group = dragGroup->clone(Entry::CloneCopy);
        }

        group->setParent(parentGroup, row);
    } else {
        if (row != -1) {
            return false;
        }

        while (!stream.atEnd()) {
            QUuid dbUuid;
            QUuid entryUuid;
            stream >> dbUuid >> entryUuid;

            Database *db = Database::databaseByUuid(dbUuid);
            if (!db) {
                continue;
            }

            Entry *dragEntry = db->rootGroup()->findEntryByUuid(entryUuid);
            if (!dragEntry || !db->rootGroup()->findEntryByUuid(dragEntry->uuid())) {
                continue;
            }

            Database *sourceDb = dragEntry->group()->database();
            Database *targetDb = parentGroup->database();

            Entry *entry = dragEntry;

            if (sourceDb != targetDb) {
                QUuid customIcon = entry->iconUuid();
                if (!customIcon.isNull() && !targetDb->metadata()->hasCustomIcon(customIcon)) {
                    targetDb->metadata()->addCustomIcon(customIcon, sourceDb->metadata()->customIcon(customIcon).data);
                }

                // Reset the UUID when moving across db boundary
                entry = dragEntry->clone(Entry::CloneDefault | Entry::CloneIncludeHistory);
                if (action == Qt::MoveAction) {
                    delete dragEntry;
                }
            } else if (action == Qt::CopyAction) {
                entry = dragEntry->clone(Entry::CloneCopy);
            }

            entry->setGroup(parentGroup);
        }
    }

    return true;
}

QStringList GroupModel::mimeTypes() const
{
    const QStringList types{
        u"application/x-keepassx-group"_s,
        u"application/x-keepassx-entry"_s,
    };
    return types;
}

QMimeData *GroupModel::mimeData(const QModelIndexList &indexes) const
{
    if (indexes.isEmpty()) {
        return nullptr;
    }

    auto data = new QMimeData();
    QByteArray encoded;
    QDataStream stream(&encoded, QIODevice::WriteOnly);

    QSet<Group *> seenGroups;

    for (const QModelIndex &index : indexes) {
        if (!index.isValid()) {
            continue;
        }

        Group *group = groupFromIndex(index);
        if (!seenGroups.contains(group)) {
            // make sure we don't add groups multiple times when we get indexes
            // with the same row but different columns
            stream << m_db->uuid() << group->uuid();
            seenGroups.insert(group);
        }
    }

    if (seenGroups.isEmpty()) {
        delete data;
        return nullptr;
    } else {
        data->setData(mimeTypes().at(0), encoded);
        return data;
    }
}

void GroupModel::groupDataChanged(Group *group)
{
    const QModelIndex ix = index(group);
    Q_EMIT dataChanged(ix, ix);
}

void GroupModel::groupAboutToRemove(Group *group)
{
    Q_ASSERT(group->parentGroup());

    QModelIndex parentIndex = parent(group);
    Q_ASSERT(parentIndex.isValid());
    const int pos = group->parentGroup()->children().indexOf(group);
    Q_ASSERT(pos != -1);

    beginRemoveRows(parentIndex, pos, pos);
}

void GroupModel::groupRemoved()
{
    endRemoveRows();
}

void GroupModel::groupAboutToAdd(Group *group, int index)
{
    Q_ASSERT(group->parentGroup());

    QModelIndex parentIndex = parent(group);

    beginInsertRows(parentIndex, index, index);
}

void GroupModel::groupAdded()
{
    endInsertRows();
}

void GroupModel::groupAboutToMove(Group *group, Group *toGroup, int pos)
{
    Q_ASSERT(group->parentGroup());

    QModelIndex oldParentIndex = parent(group);
    QModelIndex newParentIndex = index(toGroup);
    int oldPos = group->parentGroup()->children().indexOf(group);
    if (group->parentGroup() == toGroup && pos > oldPos) {
        // beginMoveRows() has a bit different semantics than Group::setParent() and
        // QList::move() when the new position is greater than the old
        pos++;
    }

    bool moveResult = beginMoveRows(oldParentIndex, oldPos, oldPos, newParentIndex, pos);
    Q_UNUSED(moveResult);
    Q_ASSERT(moveResult);
}

void GroupModel::groupMoved()
{
    endMoveRows();
}

void GroupModel::sortChildren(Group *rootGroup, bool reverse)
{
    Q_EMIT layoutAboutToBeChanged();

    QList<QModelIndex> oldIndexes;
    collectIndexesRecursively(oldIndexes, rootGroup->children());

    rootGroup->sortChildrenRecursively(reverse);

    QList<QModelIndex> newIndexes;
    collectIndexesRecursively(newIndexes, rootGroup->children());

    for (int i = 0; i < oldIndexes.count(); i++) {
        changePersistentIndex(oldIndexes[i], newIndexes[i]);
    }

    Q_EMIT layoutChanged();
}

void GroupModel::collectIndexesRecursively(QList<QModelIndex> &indexes, const QList<Group *> &groups)
{
    for (const auto &group : groups) {
        indexes.append(index(group));
        collectIndexesRecursively(indexes, group->children());
    }
}
