// SPDX-FileCopyrightText: 2010 Felix Geyer <debfx@fobos.de>
// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only

#pragma once

#include <QAbstractListModel>
#include <QSet>
#include <QtQml/qqmlregistration.h>

class Entry;
class Group;

class EntryModel : public QAbstractListModel
{
    Q_OBJECT
    QML_ELEMENT
    Q_PROPERTY(Group *group READ group WRITE setGroup NOTIFY groupChanged)

public:
    enum ExtraRole {
        ParentGroupRole = Qt::UserRole + 1,
        TitleRole,
        UsernameRole,
        UrlRole,
        DomainRole,
        ExpiryTimeRole,
        CreatedRole,
        ModifiedRole,
        AccessedRole,
        TotpRole,
        PasswordStrengthRole,
        BackgroundColorRole,
        ForegroundColorRole,
        EntryRole,
    };

    explicit EntryModel(QObject *parent = nullptr);
    Entry *entryFromIndex(const QModelIndex &index) const;
    QModelIndex indexFromEntry(Entry *entry) const;

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    QHash<int, QByteArray> roleNames() const override;

    void setGroup(Group *group);
    Group *group() const;

    void setEntries(const QList<Entry *> &entries);
    void setBackgroundColorVisible(bool visible);

    Q_INVOKABLE Entry *createEntry();
    Q_INVOKABLE void saveNewEntry();
    Q_INVOKABLE void deleteEntries(const QList<Entry *> &entries, bool force = false);

private Q_SLOTS:
    void entryAboutToAdd(Entry *entry);
    void entryAdded(Entry *entry);
    void entryAboutToRemove(Entry *entry);
    void entryRemoved();
    void entryAboutToMoveUp(int row);
    void entryMovedUp();
    void entryAboutToMoveDown(int row);
    void entryMovedDown();
    void entryDataChanged(Entry *entry);

Q_SIGNALS:
    void groupChanged();
    void askConfirmationPermanentDeletion(const QList<Entry *> &entry);

private:
    void severConnections();
    void makeConnections(const Group *group);

    bool m_backgroundColorVisible = true;
    Group *m_group = nullptr;
    QList<Entry *> m_entries;
    QList<Entry *> m_orgEntries;
    QSet<const Group *> m_allGroups;
    std::unique_ptr<Entry> m_newEntry;
};
