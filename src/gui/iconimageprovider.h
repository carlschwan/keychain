// SPDX-FileCopyrightText: 2022 Carl Schwan <carl@carlschwan.eu>
// SPDX-License-Identifier: GPL-3.0-only

#pragma once

#include <QQuickAsyncImageProvider>

#include <QNetworkAccessManager>
#include <QReadWriteLock>

class QNetworkReply;

class ThumbnailResponse : public QQuickImageResponse
{
    Q_OBJECT
public:
    explicit ThumbnailResponse(QString domain, QSize requestedSize, QNetworkAccessManager *qnam);
    ~ThumbnailResponse() override = default;

private:
    void startRequest();
    void doCancel();
    void imageQueried(QNetworkReply *reply);
    QString m_domain;
    QSize requestedSize;
    QString m_localFile;
    QNetworkAccessManager *m_qnam;
    int m_redirects;
    QList<QUrl> m_urlsToTry;

    QImage m_image;
    QString errorStr;
    mutable QReadWriteLock lock; // Guards ONLY these two members above

    QQuickTextureFactory *textureFactory() const override;
    QString errorString() const override;
    void cancel() override;
};

class IconImageProvider : public QQuickAsyncImageProvider
{
public:
    explicit IconImageProvider();
    QQuickImageResponse *requestImageResponse(const QString &domain, const QSize &requestedSize) override;

private:
    QNetworkAccessManager qnam;
};
