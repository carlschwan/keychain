// SPDX-License-Identifier: LGPL-2.0-or-later
// SPDX-FileCopyrightText: 2024 Carl Schwan <carl@carlschwan.eu>

import QtQuick
import QtQuick.Controls as Controls
import QtQuick.Layouts
import org.kde.kirigami as Kirigami
import org.kde.keychain

Kirigami.ApplicationWindow {
    id: root

    title: i18n("Plasma Keychain")

    minimumWidth: Kirigami.Units.gridUnit * 20
    minimumHeight: Kirigami.Units.gridUnit * 20

    width: Kirigami.Units.gridUnit * 55
    height: Kirigami.Units.gridUnit * 32

    globalDrawer: Sidebar {
        id: sidebar

        window: root
    }

    pageStack {
        initialPage: WelcomePage {}
        columnView.columnWidth: Controller.currentDatabase ? pageStack.defaultColumnWidth : root.width
        globalToolBar {
            style: Kirigami.ApplicationHeaderStyle.ToolBar
            showNavigationButtons: Kirigami.ApplicationHeaderStyle.ShowBackButton
        }
    }

    Connections {
        target: Controller

        function onCurrentDatabaseChanged(): void {
            if (Controller.currentDatabase) {
                while (root.pageStack.layers.depth > 1) {
                    root.pageStack.layers.pop();
                }
                while (root.pageStack.depth > 1) {
                    root.pageStack.pop();
                }
                const component = Qt.createComponent('org.kde.keychain', 'PasswordListPage');
                if (component.status === Component.Error) {
                    console.error(component.errorString());
                    return;
                }
                root.pageStack.replace(component);
            }
        }
    }
}
