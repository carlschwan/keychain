// SPDX-License-Identifier: LGPL-2.0-or-later
// SPDX-FileCopyrightText: 2024 Carl Schwan <carl@carlschwan.eu>

import QtQuick
import QtQuick.Controls as Controls
import QtQuick.Layouts
import org.kde.kirigami as Kirigami
import org.kde.kirigamiaddons.delegates as Delegates
import org.kde.kirigamiaddons.components as Components
import org.kde.kitemmodels
import org.kde.keychain

Kirigami.ScrollablePage {
    id: root

    title: Controller.currentGroup?.name ?? i18nc("@title", "No Group selected")

    header: Components.Banner {
        id: banner

        Connections {
            target: Controller

            function onShowYubiKeyPopup(): void {
                banner.text = i18nc("@info", "Please present or touch your YubiKey to continue…");
                banner.visible = true;
            }

            function onHideYubiKeyPopup(): void {
                banner.visible = false;
            }
        }
    }

    ListView {
        id: listView

        currentIndex: -1
        model: EntryModel {
            id: entryModel

            group: Controller.currentGroup

            onAskConfirmationPermanentDeletion: (entries) => {
                confirmationPermanentDeletionDialog.entries = entries;
                confirmationPermanentDeletionDialog.open();
            }
        }

        Components.MessageDialog {
            id: confirmationPermanentDeletionDialog

            property var entries

            title: i18nc("@title:dialog", "Confirm Permanent Deletion")
            dialogType: Components.MessageDialog.Warning

            Controls.Label {
                text: confirmationPermanentDeletionDialog.entries ? i18nc("@info", "Do you really want to delete permanently the following entries: %1", confirmationPermanentDeletionDialog.entries.map((entry) => '<b>' + entry.displayName + '</b>').join(', ')) : ''
                wrapMode: Text.Wrap
                Layout.fillWidth: true
            }

            standardButtons: Controls.Dialog.Cancel | Controls.Dialog.Ok

            onAccepted: {
                entryModel.deleteEntries(entries, true);
                close();
            }

            onRejected: close();
        }

        delegate: Delegates.RoundedItemDelegate {
            id: delegate

            required property int index
            required property string title
            required property string username
            required property string domain
            required property string url
            required property Entry entry

            text: domain.length > 0 ? title + ' - ' + domain : title

            contentItem: RowLayout {
                spacing: Kirigami.Units.largeSpacing

                Kirigami.Icon {
                    visible: url.length === 0
                    source: visible ? 'network-manager': ''
                    Layout.preferredWidth: 22
                    Layout.preferredHeight: 22
                }

               Image {
                    visible: url.length > 0
                    source: visible ? 'image://favicon/' + url : ''
                    Layout.preferredWidth: 22
                    Layout.preferredHeight: 22
                }

                Delegates.SubtitleContentItem {
                    itemDelegate: delegate
                    subtitle: username
                }
            }

            TapHandler {
                acceptedButtons: Qt.RightButton
                onTapped: {
                    contextMenu.entry = delegate.entry;
                    contextMenu.popup();
                }
            }

            onClicked: {
                root.Controls.ApplicationWindow.window.pageStack.push(Qt.createComponent('org.kde.keychain', 'EntryPage'), {
                    entry: delegate.entry,
                });
            }
        }

        Kirigami.PlaceholderMessage {
            width: parent.width - Kirigami.Units.gridUnit * 4
            visible: listView.count === 0
            anchors.centerIn: parent
            text: i18nc("@info:placeholder", "No passwords found in <b>%1</b>", root.title)
        }

        Controls.Menu {
            id: contextMenu

            property Entry entry

            Controls.MenuItem {
                icon.name: 'edit-copy-symbolic'
                text: i18nc("@action:inmenu", "Copy Username")
                enabled: contextMenu.entry?.username.length > 0
                onClicked: Clipboard.saveText(contextMenu.entry.username);
            }

            Controls.MenuItem {
                icon.name: 'password-copy-symbolic'
                text: i18nc("@action:inmenu", "Copy Password")
                enabled: contextMenu.entry?.password.length > 0
                onClicked: Clipboard.saveText(contextMenu.entry.password);
            }

            Controls.MenuItem {
                icon.name: 'edit-copy-symbolic'
                text: i18nc("@action:inmenu", "Copy URL")
                enabled: contextMenu.entry?.url.length > 0
                onClicked: Clipboard.saveText(contextMenu.entry.url);
            }

            Controls.MenuSeparator {}

            Controls.MenuItem {
                icon.name: 'document-edit-symbolic'
                text: i18nc("@action:inmenu", "Edit Entry")
                onClicked: root.Controls.ApplicationWindow.window.pageStack.pushDialogLayer(
                    Qt.createComponent('org.kde.keychain', 'EntryEditorPage'),
                    {
                        entry: contextMenu.entry,
                        mode: EntryEditorPage.Edit,
                    }
                );
            }

            Controls.MenuItem {
                icon.name: 'delete-symbolic'
                text: i18nc("@action:inmenu", "Delete Entry")
                onClicked: entryModel.deleteEntries([contextMenu.entry], false)
            }

            Controls.MenuItem {
                icon.name: 'document-new-symbolic'
                text: i18nc("@action:inmenu", "New Entry")
                onClicked: {
                    const page = root.Controls.ApplicationWindow.window.pageStack.pushDialogLayer(
                        Qt.createComponent('org.kde.keychain', 'EntryEditorPage'),
                        {
                            entry: entryModel.createEntry(),
                            mode: EntryEditorPage.Create,
                        }
                    );
                    page.accepted.connect(() => {
                        entryModel.saveNewEntry();
                    });
                }
            }
        }
    }
}
