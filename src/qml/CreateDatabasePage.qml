// SPDX-FileCopyrightText: 2024 Carl Schwan <carl@carlschwan.eu>
// SPDX-License-Identifier: LGPL-2.0-or-later

import QtQuick
import QtQuick.Controls as Controls
import QtQuick.Layouts
import org.kde.kirigami as Kirigami
import org.kde.kirigamiaddons.formcard as FormCard
import org.kde.coreaddons as CoreAddons
import org.kde.keychain

FormCard.FormCardPage {
    id: root

    title: i18nc("@title", "Create New Database")

    FormCard.FormHeader {
        title: i18nc("@title:group", "General Database Information")
    }

    FormCard.FormCard {
        FormCard.FormTextFieldDelegate {
            label: i18nc("@label:textbox", "Database Name:")
            text: i18nc("Initial value of the database name field", "Passwords")
        }

        FormCard.FormDelegateSeparator {}

        FormCard.FormTextFieldDelegate {
            label: i18nc("@label:textbox", "Description:")
        }
    }

    FormCard.FormHeader {
        title: i18nc("@title:group", "Encryption Settings")
    }

    FormCard.FormCard {
        FormCard.AbstractFormDelegate {
            id: decryptionTimeSlider

            text: i18nc("%1 is a time in seconds", "Decryption time: %1", CoreAddons.Format.formatDecimalDuration(slider.value))

            background: null
            contentItem: ColumnLayout {
                spacing: Kirigami.Units.smallSpacing

                Controls.Label {
                    text: decryptionTimeSlider.text
                }

                Controls.Slider {
                    id: slider

                    from: 100
                    to: 5000
                    value: 1000

                    stepSize: 100

                    Layout.fillWidth: true
                }

                RowLayout {
                    Layout.fillWidth: true

                    Controls.Label {
                        text: i18nc("minimum slider", "100ms")

                        Layout.fillWidth: true
                    }

                    Controls.Label {
                        text: i18nc("maximum slider", "5.0s")
                    }
                }

                Controls.Label {
                    Layout.fillWidth: true
                    text: i18n("Higher values offer more protection, but opening the database will take longer.")
                    color: Kirigami.Theme.disabledTextColor
                    wrapMode: Text.Wrap
                }
            }
        }

        FormCard.FormDelegateSeparator {}

        FormCard.FormComboBoxDelegate {
            text: i18nc("@label:textbox", "Database Format:")
            textRole: "text"
            currentIndex: 0
            model: [
                { text: i18nc("@label:listbox", "KDBX 4 (recommanded)") },
                { text: i18nc("@label:listbox", "KDBX 3") }
            ]
            description: i18n("Unless you need to open your database with other programs, always use the latest pformat.")
        }
    }

    FormCard.FormHeader {
        title: i18nc("@title:group", "Database Credentials")
    }

    FormCard.FormCard {
        FormCard.FormPasswordFieldDelegate {
            label: i18nc("@label:textbox", "Password:")
        }

        FormCard.FormDelegateSeparator {}

        FormCard.FormPasswordFieldDelegate {
            label: i18nc("@label:textbox", "Confirm Password:")
        }
    }

    FormCard.FormHeader {
        title: i18nc("@title:group", "Key File (optional)")
    }

    FormCard.FormCard {
        FormCard.FormSectionText {
            text: i18nc("@info:whatsis", "You can add a key file containing random bytes for additional security. You must keep it secret and never loose it or you will be locked out.")
        }

        FormCard.FormDelegateSeparator {}

        FormCard.FormButtonDelegate {
            text: i18nc("@action:button", "Generate File")
        }

        FormCard.FormDelegateSeparator {}

        FormCard.FormButtonDelegate {
            text: i18nc("@action:button", "Choose Existing File")
        }
    }

    FormCard.FormHeader {
        title: i18nc("@title:group", "Challenge-Response (optional)")
    }

    FormCard.FormCard {
        FormCard.FormSectionText {
            text: i18nc("@info:whatsis", "If you own a <a href=\"\">YubiKey</a> or a <a href=\"\">OnlyKey</a>, you can use it for additional security. The key requires one of its slot to be programmed as <a href=\"\">HMAC-SHA1 Challenge-Response</a>.")
        }

        FormCard.FormDelegateSeparator {}

        FormCard.FormComboBoxDelegate {
            id: hardwareKeyCombo

            text: i18nc("@label:listbox", "Hardware key:")
            model: YubikeySlotModel {
                onToggleHardwareKeyComponent: (state) => {
                    if (!state) {
                        hardwareKeyCombo.currentIndex = 0;
                    }
                }
            }
            textRole: "name"
            valueRole: "value"
            enabled: count > 1
            displayText: currentIndex === 0 ? i18nc("@info:placeholder", "Select hardware key...") : currentText
            currentIndex: 0
            onCurrentValueChanged: () =>{
                if (currentIndex === 0) {
                    databaseOpenHelper.yubikeySlot = undefined;
                } else {
                    databaseOpenHelper.yubikeySlot = currentValue;
                }
            }
        }
    }

    footer: Controls.ToolBar {
        contentItem: Controls.DialogButtonBox {
            standardButtons: Controls.Dialog.Ok | Controls.Dialog.Cancel
        }
    }
}
