// SPDX-FileCopyrightText: 2024 Carl Schwan <carl@carlschwan.eu>
// SPDX-License-Identifier: LGPL-2.0-or-later

import QtCore
import QtQuick
import QtQuick.Dialogs
import QtQuick.Layouts
import QtQuick.Controls as Controls
import org.kde.kirigami as Kirigami
import org.kde.kirigamiaddons.formcard as FormCard

/// Welcome page
FormCard.FormCardPage {
    id: root

    Kirigami.Icon {
        source: "org.kde.keychain"
        implicitWidth: Math.round(Kirigami.Units.iconSizes.huge * 1.5)
        implicitHeight: Math.round(Kirigami.Units.iconSizes.huge * 1.5)

        Layout.alignment: Qt.AlignHCenter
        Layout.topMargin: Kirigami.Units.largeSpacing * 4
    }

    Kirigami.Heading {
        text: i18n("Welcome to Plasma Keychain")

        Layout.alignment: Qt.AlignHCenter
        Layout.topMargin: Kirigami.Units.largeSpacing
        Layout.bottomMargin: Kirigami.Units.largeSpacing * 4
    }

    FormCard.FormCard {
        FormCard.FormButtonDelegate {
            text: i18nc("@action:button", "Create new database")
            onClicked: {
                Controls.ApplicationWindow.window.pageStack.push(Qt.createComponent("org.kde.keychain", "CreateDatabasePage"))
            }
        }

        FormCard.FormDelegateSeparator {}

        FormCard.FormButtonDelegate {
            text: i18nc("@action:button", "Open existing database")
            onClicked: {
                fileDialog.open();
            }

            FileDialog {
                id: fileDialog
                title: i18n("Please choose a file")
                currentFolder: StandardPaths.writableLocation(StandardPaths.DocumentsLocation)
                nameFilters: [
                    i18nc("@item:inlistbox File dialog filter", "KeePass Database (*.kdbx)"),
                    i18nc("@item:inlistbox File dialog filter", "All Files (*)")
                ]
                onAccepted: {
                    root.Controls.ApplicationWindow.window.pageStack.push(Qt.createComponent("org.kde.keychain", "UnlockPage"), {
                        databaseUrl: selectedFile,
                    });
                }
            }
        }
    }
}