/*
 *  Copyright (C) 2023 KeePassXC Team <team@keepassxc.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 or (at your option)
 *  version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "OPUXReader.h"
#include "core/Database.h"
#include "core/Entry.h"
#include "core/Group.h"
#include "core/Metadata.h"
#include "core/Totp.h"
#include <KLocalizedString>

#include <QFileInfo>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QScopedPointer>
#include <QUrl>

#include <minizip/unzip.h>

namespace
{
    QByteArray extractFile(unzFile uf, QString filename)
    {
        if (unzLocateFile(uf, filename.toLatin1().data(), 2) != UNZ_OK) {
            qWarning("Failed to extract 1PUX document: %s", qPrintable(filename));
            return {};
        }

        // Read export.data into memory
        int bytes, bytesRead = 0;
        QByteArray data;
        unzOpenCurrentFile(uf);
        do {
            data.resize(data.size() + 8192);
            bytes = unzReadCurrentFile(uf, data.data() + bytesRead, 8192);
            if (bytes > 0) {
                bytesRead += bytes;
            }
        } while (bytes > 0);
        unzCloseCurrentFile(uf);
        data.truncate(bytesRead);

        return data;
    }

    Entry* readItem(const QJsonObject& item, unzFile uf = nullptr)
    {
        const auto itemMap = item.toVariantMap();
        const auto overviewMap = itemMap.value(QLatin1StringView("overview")).toMap();
        const auto detailsMap = itemMap.value(QLatin1StringView("details")).toMap();

        // Create entry and assign basic values
        QScopedPointer<Entry> entry(new Entry());
        entry->setUuid(QUuid::createUuid());
        entry->setTitle(overviewMap.value(QLatin1StringView("title")).toString());
        entry->setUrl(overviewMap.value(QLatin1StringView("url")).toString());
        if (overviewMap.contains(QLatin1StringView("urls"))) {
            int i = 1;
            for (const auto& urlRaw : overviewMap.value(QLatin1StringView("urls")).toList()) {
                const auto urlMap = urlRaw.toMap();
                const auto url = urlMap.value(QLatin1StringView("url")).toString();
                if (entry->url() != url) {
                    entry->attributes()->set(
                        QStringLiteral("%1_%2").arg(EntryAttributes::AdditionalUrlAttribute, QString::number(i)), url);
                    ++i;
                }
            }
        }
        if (overviewMap.contains(QLatin1StringView("tags"))) {
            entry->setTags(overviewMap.value(QLatin1StringView("tags")).toStringList().join(u','));
        }
        if (itemMap.value(QLatin1StringView("favIndex")).toString() == QLatin1StringView("1")) {
            entry->addTag(i18nc("Tag for favorite entries", "Favorite"));
        }
        if (itemMap.value(QLatin1StringView("state")).toString() == QLatin1StringView("archived")) {
            entry->addTag(i18nc("Tag for archived entries", "Archived"));
        }

        // Parse the details map by setting the username, password, and notes first
        const auto loginFields = detailsMap.value(QLatin1StringView("loginFields")).toList();
        for (const auto& field : loginFields) {
            const auto fieldMap = field.toMap();
            const auto designation = fieldMap.value(QLatin1StringView("designation")).toString();
            if (designation.compare(QLatin1StringView("username"), Qt::CaseInsensitive) == 0) {
                entry->setUsername(fieldMap.value(QLatin1StringView("value")).toString());
            } else if (designation.compare(QLatin1StringView("password"), Qt::CaseInsensitive) == 0) {
                entry->setPassword(fieldMap.value(QLatin1StringView("value")).toString());
            }
        }
        entry->setNotes(detailsMap.value(QLatin1StringView("notesPlain")).toString());

        // Dive into the item sections to pull out advanced attributes
        const auto sections = detailsMap.value(QLatin1StringView("sections")).toList();
        for (const auto& section : sections) {
            // Derive a prefix for attribute names using the title or uuid if missing
            const auto sectionMap = section.toMap();
            auto prefix = sectionMap.value(QLatin1StringView("title")).toString();
            if (prefix.isEmpty()) {
                prefix = QUuid::createUuid().toString().mid(1, 5);
            }

            for (const auto& field : sectionMap.value(QLatin1StringView("fields")).toList()) {
                // Form the name of the attribute using the prefix and title or id
                const auto fieldMap = field.toMap();
                auto name = fieldMap.value(QLatin1StringView("title")).toString();
                if (name.isEmpty()) {
                    name = fieldMap.value(QLatin1StringView("id")).toString();
                }
                name = QStringLiteral("%1_%2").arg(prefix, name);

                const auto valueMap = fieldMap.value(QLatin1StringView("value")).toMap();
                const auto key = valueMap.firstKey();
                if (key == QLatin1StringView("totp")) {
                    auto totp = valueMap.value(key).toString();
                    if (!totp.startsWith(QLatin1StringView("otpauth://"))) {
                        // Build otpauth url
                        QUrl url(QStringLiteral("otpauth://totp/%1:%2?secret=%3")
                                     .arg(QString::fromUtf8(QUrl::toPercentEncoding(entry->title())),
                                          QString::fromUtf8(QUrl::toPercentEncoding(entry->username())),
                                          QString::fromUtf8(QUrl::toPercentEncoding(totp))));
                        totp = url.toString(QUrl::FullyEncoded);
                    }

                    if (entry->hasTotp()) {
                        // Store multiple TOTP definitions as additional otp attributes
                        int i = 0;
                        name = QLatin1StringView("otp");
                        const auto attributes = entry->attributes()->keys();
                        while (attributes.contains(name)) {
                            name = QString(QLatin1StringView("otp_%1")).arg(++i);
                        }
                        entry->attributes()->set(name, totp, true);
                    } else {
                        // First otp value encountered gets formal storage
                        entry->setTotp(Totp::parseSettings(totp));
                    }
                } else if (key == QLatin1StringView("file")) {
                    // Add a file to the entry attachments
                    const auto fileMap = valueMap.value(key).toMap();
                    const auto fileName = fileMap.value(QLatin1StringView("fileName")).toString();
                    const auto docId = fileMap.value(QLatin1StringView("documentId")).toString();
                    const auto data = extractFile(uf, QStringLiteral("files/%1__%2").arg(docId, fileName));
                    if (!data.isNull()) {
                        entry->attachments()->set(fileName, data);
                    }
                } else {
                    auto value = valueMap.value(key).toString();
                    if (key == QLatin1StringView("date")) {
                        // Convert date fields from Unix time
                        value = QDateTime::fromSecsSinceEpoch(valueMap.value(key).toULongLong(), Qt::UTC).toString();
                    } else if (key == QLatin1StringView("email")) {
                        // Email address is buried in a sub-value
                        value = valueMap.value(key).toMap().value(QLatin1StringView("email_address")).toString();
                    } else if (key == QLatin1StringView("address")) {
                        // Combine all the address attributes into a fully formed structure
                        const auto address = valueMap.value(key).toMap();
                        value = address.value(QLatin1StringView("street")).toString() + u'\n' + address.value(QLatin1StringView("city")).toString() + QLatin1StringView(", ")
                                + address.value(QLatin1StringView("state")).toString() + u' ' + address.value(QLatin1StringView("zip")).toString() + u'\n'
                                + address.value(QLatin1StringView("country")).toString();
                    }

                    if (!value.isEmpty()) {
                        entry->attributes()->set(name, value, key == QLatin1StringView("concealed"));
                    }
                }
            }
        }

        // Add a document attachment if defined
        if (detailsMap.contains(QLatin1StringView("documentAttributes"))) {
            const auto document = detailsMap.value(QLatin1StringView("documentAttributes")).toMap();
            const auto fileName = document.value(QLatin1StringView("fileName")).toString();
            const auto docId = document.value(QLatin1StringView("documentId")).toString();
            const auto data = extractFile(uf, QStringLiteral("files/%1__%2").arg(docId, fileName));
            if (!data.isNull()) {
                entry->attachments()->set(fileName, data);
            }
        }

        // Collapse any accumulated history
        entry->removeHistoryItems(entry->historyItems());

        // Adjust the created and modified times
        auto timeInfo = entry->timeInfo();
        const auto createdTime = QDateTime::fromSecsSinceEpoch(itemMap.value(QLatin1StringView("createdAt")).toULongLong(), Qt::UTC);
        const auto modifiedTime = QDateTime::fromSecsSinceEpoch(itemMap.value(QLatin1StringView("updatedAt")).toULongLong(), Qt::UTC);
        timeInfo.setCreationTime(createdTime);
        timeInfo.setLastModificationTime(modifiedTime);
        timeInfo.setLastAccessTime(modifiedTime);
        entry->setTimeInfo(timeInfo);

        return entry.take();
    }

    void writeVaultToDatabase(const QJsonObject& vault, QSharedPointer<Database> db, unzFile uf = nullptr)
    {
        if (!vault.contains(QLatin1StringView("attrs")) || !vault.contains(QLatin1StringView("items"))) {
            // Early out if the vault is missing critical items
            return;
        }

        const auto attr = vault.value(QLatin1StringView("attrs")).toObject().toVariantMap();

        // Create group and assign basic values
        auto group = new Group();
        group->setUuid(QUuid::createUuid());
        group->setName(attr.value(QLatin1StringView("name")).toString());
        group->setParent(db->rootGroup());

        const auto items = vault.value(QLatin1StringView("items")).toArray();
        for (const auto& item : items) {
            auto entry = readItem(item.toObject(), uf);
            if (entry) {
                entry->setGroup(group, false);
            }
        }

        // Add the group icon if present
        const auto icon = attr.value(QLatin1StringView("avatar")).toString();
        if (!icon.isEmpty()) {
            auto data = extractFile(uf, QStringLiteral("files/%1").arg(icon));
            if (!data.isNull()) {
                const auto uuid = QUuid::createUuid();
                db->metadata()->addCustomIcon(uuid, data);
                group->setIconUuid(uuid);
            }
        }
    }
} // namespace

bool OPUXReader::hasError()
{
    return !m_error.isEmpty();
}

QString OPUXReader::errorString()
{
    return m_error;
}

QSharedPointer<Database> OPUXReader::convert(const QString& path)
{
    m_error.clear();

    QFileInfo fileinfo(path);
    if (!fileinfo.exists()) {
        m_error = i18n("File does not exist.", path);
        return {};
    }

    // 1PUX is a zip file format, open it and process the contents in memory
    auto uf = unzOpen64(fileinfo.absoluteFilePath().toLatin1().constData());
    if (!uf) {
        m_error = i18n("Invalid 1PUX file format: Not a valid ZIP file.");
        return {};
    }

    // Find the export.data file, if not found this isn't a 1PUX file
    auto data = extractFile(uf, QStringLiteral("export.data"));
    if (data.isNull()) {
        m_error = i18n("Invalid 1PUX file format: Missing export.data");
        unzClose(uf);
        return {};
    }

    auto db = QSharedPointer<Database>::create();
    db->rootGroup()->setName(i18n("1Password Import"));
    const auto json = QJsonDocument::fromJson(data);

    const auto account = json.object().value(QLatin1StringView("accounts")).toArray().first().toObject();
    const auto vaults = account.value(QLatin1StringView("vaults")).toArray();

    for (const auto& vault : vaults) {
        writeVaultToDatabase(vault.toObject(), db, uf);
    }

    unzClose(uf);
    return db;
}
