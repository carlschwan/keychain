/*
 *  Copyright (C) 2023 KeePassXC Team <team@keepassxc.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 or (at your option)
 *  version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "BitwardenReader.h"

#include "core/Database.h"
#include "core/Entry.h"
#include "core/Group.h"
#include "core/Metadata.h"
#include "core/Tools.h"
#include "core/Totp.h"
#include "crypto/CryptoHash.h"
#include "crypto/SymmetricCipher.h"
#include "crypto/kdf/Argon2Kdf.h"

#include <botan/kdf.h>
#include <botan/pwdhash.h>

#include <KLocalizedString>

#include <QFileInfo>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonParseError>
#include <QMap>
#include <QScopedPointer>
#include <QUrl>

namespace
{
    Entry* readItem(const QJsonObject& item, QString& folderId)
    {
        // Create the item map and extract the folder id
        const auto itemMap = item.toVariantMap();
        folderId = itemMap.value(QLatin1StringView("folderId")).toString();
        if (folderId.isEmpty()) {
            // Bitwarden organization vaults use collectionId instead of folderId
            auto collectionIds = itemMap.value(QLatin1StringView("collectionIds")).toStringList();
            if (!collectionIds.empty()) {
                folderId = collectionIds.first();
            }
        }

        // Create entry and assign basic values
        QScopedPointer<Entry> entry(new Entry());
        entry->setUuid(QUuid::createUuid());
        entry->setTitle(itemMap.value(QStringLiteral("name")).toString());
        entry->setNotes(itemMap.value(QStringLiteral("notes")).toString());

        if (itemMap.value(QStringLiteral("favorite")).toBool()) {
            entry->addTag(i18nc("Favorite", "Tag for favorite entries"));
        }

        // Parse login details if present
        if (itemMap.contains(QStringLiteral("login"))) {
            const auto loginMap = itemMap.value(QStringLiteral("login")).toMap();
            entry->setUsername(loginMap.value(QStringLiteral("username")).toString());
            entry->setPassword(loginMap.value(QStringLiteral("password")).toString());
            if (loginMap.contains(QStringLiteral("totp"))) {
                auto totp = loginMap.value(QStringLiteral("totp")).toString();
                if (!totp.startsWith(QStringLiteral("otpauth://"))) {
                    QUrl url(QStringLiteral("otpauth://totp/%1:%2?secret=%3")
                                 .arg(QString::fromUtf8(QUrl::toPercentEncoding(entry->title())),
                                      QString::fromUtf8(QUrl::toPercentEncoding(entry->username())),
                                      QString::fromUtf8(QUrl::toPercentEncoding(totp))));
                    totp = url.toString(QUrl::FullyEncoded);
                }
                entry->setTotp(Totp::parseSettings(totp));
            }

            // Set the entry url(s)
            int i = 1;
            const auto urlObjs = loginMap.value(QStringLiteral("uris")).toList();
            for (const auto &urlObj : urlObjs) {
                const auto url = urlObj.toMap().value(QStringLiteral("uri")).toString();
                if (entry->url().isEmpty()) {
                    // First url encountered is set as the primary url
                    entry->setUrl(url);
                } else {
                    // Subsequent urls
                    entry->attributes()->set(
                        QStringLiteral("%1_%2").arg(EntryAttributes::AdditionalUrlAttribute, QString::number(i)), url);
                    ++i;
                }
            }
        }

        // Parse identity details if present
        if (itemMap.contains(QStringLiteral("identity"))) {
            const auto idMap = itemMap.value(QStringLiteral("identity")).toMap();

            // Combine name attributes
            auto attrs = QStringList({idMap.value(QStringLiteral("title")).toString(),
                                      idMap.value(QStringLiteral("firstName")).toString(),
                                      idMap.value(QStringLiteral("middleName")).toString(),
                                      idMap.value(QStringLiteral("lastName")).toString()});
            attrs.removeAll(QString());
            entry->attributes()->set(QStringLiteral("identity_name"), attrs.join(u' '));

            // Combine all the address attributes
            attrs = QStringList({idMap.value(QStringLiteral("address1")).toString(),
                                 idMap.value(QStringLiteral("address2")).toString(),
                                 idMap.value(QStringLiteral("address3")).toString()});
            attrs.removeAll(QString());
            QString address = attrs.join(u'\n') + u'\n' + idMap.value(QStringLiteral("city")).toString() + QStringLiteral(", ")
                + idMap.value(QStringLiteral("state")).toString() + u' ' + idMap.value(QStringLiteral("postalCode")).toString() + QStringLiteral("\n")
                + idMap.value(QStringLiteral("country")).toString();
            entry->attributes()->set(QStringLiteral("identity_address"), address);

            // Add the remaining attributes
            attrs = QStringList({QStringLiteral("company"), QStringLiteral("email"), QStringLiteral("phone"), QStringLiteral("ssn"), QStringLiteral("passportNumber"), QStringLiteral("licenseNumber")});
            const QStringList sensitive({QStringLiteral("ssn"), QStringLiteral("passportNumber"), QStringLiteral("licenseNumber")});
            for (const auto &attr : std::as_const(attrs)) {
                const auto value = idMap.value(attr).toString();
                if (!value.isEmpty()) {
                    entry->attributes()->set(QStringLiteral("identity_") + attr, value, sensitive.contains(attr));
                }
            }

            // Set the username or push it into attributes if already set
            const auto username = idMap.value(QLatin1StringView("username")).toString();
            if (!username.isEmpty()) {
                if (entry->username().isEmpty()) {
                    entry->setUsername(username);
                } else {
                    entry->attributes()->set(QLatin1StringView("identity_username"), username);
                }
            }
        }

        // Parse card details if present
        if (itemMap.contains(QStringLiteral("card"))) {
            const auto cardMap = itemMap.value(QStringLiteral("card")).toMap();
            const QStringList attrs({QStringLiteral("cardholderName"), QStringLiteral("brand"), QStringLiteral("number"), QStringLiteral("expMonth"), QStringLiteral("expYear"), QStringLiteral("code")});
            const QStringList sensitive({QStringLiteral("code")});
            for (const auto& attr : attrs) {
                auto value = cardMap.value(attr).toString();
                if (!value.isEmpty()) {
                    entry->attributes()->set(QStringLiteral("card_") + attr, value, sensitive.contains(attr));
                }
            }
        }

        // Parse remaining fields
        for (const auto& field : itemMap.value(QStringLiteral("fields")).toList()) {
            // Derive a prefix for attribute names using the title or uuid if missing
            const auto fieldMap = field.toMap();
            auto name = fieldMap.value(QStringLiteral("name")).toString();
            if (entry->attributes()->hasKey(name)) {
                name = QStringLiteral("%1_%2").arg(name, QUuid::createUuid().toString().mid(1, 5));
            }

            const auto value = fieldMap.value(QStringLiteral("value")).toString();
            const auto type = fieldMap.value(QStringLiteral("type")).toInt();

            entry->attributes()->set(name, value, type == 1);
        }

        // Collapse any accumulated history
        entry->removeHistoryItems(entry->historyItems());

        return entry.take();
    }

    void writeVaultToDatabase(const QJsonObject& vault, QSharedPointer<Database> db)
    {
        auto folderField = QStringLiteral("folders");
        if (!vault.contains(folderField)) {
            // Handle Bitwarden organization vaults
            folderField = QStringLiteral("collections");
        }

        if (!vault.contains(folderField) || !vault.contains(QLatin1StringView("items"))) {
            // Early out if the vault is missing critical items
            return;
        }

        // Create groups from folders and store a temporary map of id -> uuid
        QMap<QString, Group*> folderMap;
        const auto folders = vault.value(folderField).toArray();
        for (const auto &folder : folders) {
            auto group = new Group();
            group->setUuid(QUuid::createUuid());
            group->setName(folder.toObject().value(QLatin1StringView("name")).toString());
            group->setParent(db->rootGroup());

            folderMap.insert(folder.toObject().value(QLatin1StringView("id")).toString(), group);
        }

        QString folderId;
        const auto items = vault.value(QLatin1StringView("items")).toArray();
        for (const auto& item : items) {
            auto entry = readItem(item.toObject(), folderId);
            if (entry) {
                entry->setGroup(folderMap.value(folderId, db->rootGroup()), false);
            }
        }
    }
} // namespace

bool BitwardenReader::hasError()
{
    return !m_error.isEmpty();
}

QString BitwardenReader::errorString()
{
    return m_error;
}

QSharedPointer<Database> BitwardenReader::convert(const QString& path, const QString& password)
{
    m_error.clear();

    QFileInfo fileinfo(path);
    if (!fileinfo.exists()) {
        m_error = i18n("File does not exist.", path);
        return {};
    }

    // Bitwarden uses a json file format
    QFile file(fileinfo.absoluteFilePath());
    if (!file.open(QFile::ReadOnly)) {
        m_error = i18n("Cannot open file: %1", file.errorString());
        return {};
    }

    QJsonParseError error;
    auto json = QJsonDocument::fromJson(file.readAll(), &error).object();
    if (error.error != QJsonParseError::NoError) {
        m_error = i18n("Cannot parse file: %1 at position %2", error.errorString(), QString::number(error.offset));
        return {};
    }

    file.close();

    // Check if this is an encrypted json
    if (json.contains(QLatin1StringView("encrypted")) && json.value(QLatin1StringView("encrypted")).toBool()) {
        auto buildError = [](const QString &errorString) {
            return i18n("Failed to decrypt json file: %1", errorString);
        };

        QByteArray key(32, '\0');
        auto salt = json.value(QLatin1StringView("salt")).toString().toUtf8();
        auto kdfType = json.value(QLatin1StringView("kdfType")).toInt();

        // Derive the Master Key
        if (kdfType == 0) {
            auto pwd_fam = Botan::PasswordHashFamily::create_or_throw("PBKDF2(SHA-256)");
            auto pwd_hash = pwd_fam->from_params(json.value(QLatin1StringView("kdfIterations")).toInt());
            pwd_hash->derive_key(reinterpret_cast<uint8_t*>(key.data()),
                                 key.size(),
                                 password.toUtf8().data(),
                                 password.toUtf8().size(),
                                 reinterpret_cast<uint8_t*>(salt.data()),
                                 salt.size());
        } else if (kdfType == 1) {
            // Bitwarden hashes the salt for Argon2 for some reason
            CryptoHash saltHash(CryptoHash::Sha256);
            saltHash.addData(salt);
            salt = saltHash.result();

            Argon2Kdf argon2(Argon2Kdf::Type::Argon2id);
            argon2.setSeed(salt);
            argon2.setRounds(json.value(QLatin1StringView("kdfIterations")).toInt());
            argon2.setMemory(json.value(QLatin1StringView("kdfMemory")).toInt() * 1024);
            argon2.setParallelism(json.value(QLatin1StringView("kdfParallelism")).toInt());
            argon2.transform(password.toUtf8(), key);
        } else {
            m_error = buildError(i18n("Unsupported KDF type, cannot decrypt json file"));
            return {};
        }

        auto hkdf = Botan::KDF::create_or_throw("HKDF-Expand(SHA-256)");

        // Derive the MAC Key
        auto stretched_mac = hkdf->derive_key(32, reinterpret_cast<const uint8_t*>(key.data()), key.size(), "", "mac");
        auto mac = QByteArray(reinterpret_cast<const char*>(stretched_mac.data()), stretched_mac.size());

        // Stretch the Master Key
        auto stretched_key = hkdf->derive_key(32, reinterpret_cast<const uint8_t*>(key.data()), key.size(), "", "enc");
        key = QByteArray(reinterpret_cast<const char*>(stretched_key.data()), stretched_key.size());

        // Validate the encryption key
        auto keyList = json.value(QLatin1StringView("encKeyValidation_DO_NOT_EDIT")).toString().split(u'.');
        if (keyList.size() < 2) {
            m_error = buildError(i18n("Invalid encKeyValidation field"));
            return {};
        }
        auto cipherList = keyList[1].split(u'|');
        if (cipherList.size() < 3) {
            m_error = buildError(i18n("Invalid cipher list within encKeyValidation field"));
            return {};
        }
        CryptoHash hash(CryptoHash::Sha256, true);
        hash.setKey(mac);
        hash.addData(QByteArray::fromBase64(cipherList[0].toUtf8())); // iv
        hash.addData(QByteArray::fromBase64(cipherList[1].toUtf8())); // ciphertext
        if (hash.result().toBase64() != cipherList[2].toUtf8()) {
            // Calculated MAC doesn't equal the Validation
            m_error = buildError(i18n("Wrong password"));
            return {};
        }

        // Decrypt data field using AES-256-CBC
        keyList = json.value(QLatin1StringView("data")).toString().split(u'.');
        if (keyList.size() < 2) {
            m_error = buildError(i18n("Invalid encrypted data field"));
            return {};
        }
        cipherList = keyList[1].split(u'|');
        if (cipherList.size() < 2) {
            m_error = buildError(i18n("Invalid cipher list within encrypted data field"));
            return {};
        }
        auto iv = QByteArray::fromBase64(cipherList[0].toUtf8());
        auto data = QByteArray::fromBase64(cipherList[1].toUtf8());

        SymmetricCipher cipher;
        if (!cipher.init(SymmetricCipher::Aes256_CBC, SymmetricCipher::Decrypt, key, iv)) {
            m_error = buildError(i18n("Cannot initialize cipher"));
            return {};
        }
        if (!cipher.finish(data)) {
            m_error = buildError(i18n("Cannot decrypt data"));
            return {};
        }

        json = QJsonDocument::fromJson(data, &error).object();
        if (error.error != QJsonParseError::NoError) {
            m_error = buildError(error.errorString());
            return {};
        }
    }

    auto db = QSharedPointer<Database>::create();
    db->rootGroup()->setName(i18n("Bitwarden Import"));

    writeVaultToDatabase(json, db);

    return db;
}
